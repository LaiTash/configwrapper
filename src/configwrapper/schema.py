import ConfigParser
from itertools import ifilter
import io
from configwrapper import ValidationError
from configwrapper.section import ConfigSection
import tempfile

__author__ = 'Lai Tash (lai.tash@yandex.ru)'


#class _ConfigSchemaMeta(type):
#    def __init__(cls, *args, **kwargs):
#        for key, member in cls.__dict__.iteritems():
#            if (isinstance(member, type) and
#                    issubclass(member, ConfigSection)):
#                obj = member()
#                obj.schema = cls
#                setattr(cls, key, )


class ConfigSchema(object):

    allow_undefined = False

    def __init__(self, config, allow_undefined=None, **sections):
        if allow_undefined is not None:
            self.allow_undefined = allow_undefined
        self.config = config
        for name in dir(self):
            member = getattr(self, name)
            if isinstance(member, type) and issubclass(member, ConfigSection):
                obj = member()
                obj.bind(schema=self)
                if obj.prefix is None:
                    obj.bind(prefix=name)
                setattr(self, name, obj)
            elif isinstance(member, ConfigSection):
                member.bind(schema=self)
                if member.prefix is None:
                    member.bind(prefix=name)
        for key, section in sections.iteritems():
            section.bind(schema=self)
            if section.prefix is None:
                section.bind(prefix=key)
            setattr(self, key, section)

    @classmethod
    def load(cls, filenames, **kwargs):
        """ Load the configuration from file and return a schema object.

        Example::
            from configwrapper.schema import ConfigSchema
            ConfigSchema.load('example_file', allow_undefined=True)

        Example with temporary file::
            >>> from configwrapper.schema import ConfigSchema
            >>> from os import unlink
            >>> from tempfile import NamedTemporaryFile

            >>> suffix='config_example'
            >>> config_example_fileobj = NamedTemporaryFile(
            ...        suffix=suffix, delete=False
            ... )
            >>> config_example_fileobj.write('[section1]')
            >>> config_example_fileobj.close()

            >>> schema = ConfigSchema.load(config_example_fileobj.name)
            >>> print schema.config.sections()
            ['section1']
            >>> unlink(config_example_fileobj.name)

        :param filenames: Name(s) of the configuration file to load.
        :rtype: str|list[str]
        :param kwargs: ConfigSchema keyword arguments, if any.
        :return: Schema object
        :rtype: isinstance(ConfigSchema)
        """
        config = ConfigParser.ConfigParser()
        config.read(filenames)
        schema = cls(config, **kwargs)
        return schema

    @staticmethod
    def _clean_config_string(source):
        """ Left strip spaces from each line of the config string.
        :return: stripped source.
        """
        return '\n'.join(map(str.lstrip, source.split('\n')))

    @classmethod
    def read(cls, source, strip_spaces=False, **kwargs):
        """ Read the configuration from a string and return a schema object.

        Example::
            >>> from configwrapper.schema import ConfigSchema
            >>> config = "[section]"
            >>> schema = ConfigSchema.read(config)
            >>> print schema.config.sections()
            ['section']

        :param source: Source configuration string.
        :param strip_spaces: If True, left strip each line.
        :type strip_spaces: bool
        :param kwargs: ConfigSchema keyword arguments, if any.
        :return: Schema object
        :rtype: isinstance(ConfigSchema)
        """
        config = ConfigParser.ConfigParser()
        if strip_spaces:
            source = cls._clean_config_string(source)
        fp = io.BytesIO(source)
        config.readfp(fp)
        return cls(config, **kwargs)

    @property
    def defined_sections(self):
        """ Return defined sections for this schema.

        Example::
            >>> from configwrapper.schema import ConfigSchema, ConfigSection
            >>> class Schema(ConfigSchema):
            ...     section1 = ConfigSection()
            ...     section2 = ConfigSection()
            ...
            >>> schema = Schema(None)
            >>> print schema.defined_sections
            <generator object ...>
            >>> print [section.prefix for section in schema.defined_sections]
            ['section1', 'section2']

        :return: Generator of sections.
        """

        for key in dir(self):
            member = getattr(self, key)
            if isinstance(member, ConfigSection):
                yield member

    def validate(self):
        """ Validate configuration according to the schema.

        :raises ValidationError: something goes wrong...
        """


        for section_name in self.config.sections():
            section = (s.match_name(section_name)
                       for s in self.defined_sections)
            section = ifilter(None, section)
            section = next(section, None)

            if section is None:
                if not self.allow_undefined:
                    raise ValidationError("Undefined section: '%s'" %
                                          section_name)
            else:
                section.validate()
