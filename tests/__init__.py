import ConfigParser
import io


def load_conf(config):
    result = ConfigParser.RawConfigParser()
    config = '\n'.join([item.lstrip() for item in config.split('\n')])
    result.readfp(io.BytesIO(config))
    return result