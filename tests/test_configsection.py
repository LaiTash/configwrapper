import ConfigParser
import io
from unittest import TestCase
from configwrapper import ValidationError
from configwrapper.options.numeric import IntOption

from configwrapper.options.strings import StringOption
from configwrapper.section import ConfigSection, TemplateSection, SectionError
from configwrapper.schema import ConfigSchema


class TestSingletonSection(TestCase):
    def setUp(self):
        self.config = ConfigParser.RawConfigParser()

    def load_conf(self, config):
        config = '\n'.join([item.lstrip() for item in config.split('\n')])
        self.config.readfp(io.BytesIO(config))
        return self.config

    def test_get_serialized(self):
        class Schema(ConfigSchema):
            section = ConfigSection(opt1=StringOption())

        config = """
        [section]
        opt1=value
        """

        schema = Schema(self.load_conf(config))

        self.assertEqual(schema.section.get_serialized('opt1'), 'value')
        self.assertRaises(ConfigParser.NoOptionError,
                          schema.section.get_serialized, 'opt2')

    def test_validate_undefined(self):
        config = """
        [section]
        a = something
        b = otherthing
        """

        class Schema(ConfigSchema):
            section = ConfigSection()

        self.assertRaises(ValidationError,
                          Schema(self.load_conf(config)).section.validate)

        class Schema(ConfigSchema):
            section = ConfigSection(allow_undefined=True)

        Schema(self.load_conf(config)).section.validate()

    def test_validate_options(self):
        config = """
        [section]
        a =
        b = notint
        """

        class Schema(ConfigSchema):
            section = ConfigSection(
                a=StringOption(nullable=False),
                b=IntOption()
            )

        self.assertRaises(ValidationError,
                          Schema(self.load_conf(config)).section.validate)


    def test_match_name(self):
        section = ConfigSection(prefix="section")
        self.assertEqual(section.match_name('section'), section)
        self.assertIsNone(section.match_name('notme'))

    def test_name_unchanged(self):
        class Section(ConfigSection):
            opt = StringOption(name='replaced')

        class Schema(ConfigSchema):
            section = Section

        config = """
        [section]
        replaced=ok
        """

        schema = Schema(self.load_conf(config))

        self.assertEqual(schema.section.opt, 'ok')


class TestTemplateSection(TestCase):
    def setUp(self):
        self.config = ConfigParser.RawConfigParser()

    def load_conf(self, config):
        config = '\n'.join([item.lstrip() for item in config.split('\n')])
        self.config.readfp(io.BytesIO(config))
        return self.config

    def test_get_method(self):
        section_cls = TemplateSection()

        class Schema(ConfigSchema):
            section = section_cls

        config = """
        [section]
        [section_one]
        """

        schema = Schema(self.load_conf(config))
        self.assertEqual(schema.section, section_cls)
        self.assertRaises(AttributeError, lambda: schema.section_one)

    def test_getitem(self):

        section_cls = TemplateSection()

        class Schema(ConfigSchema):
            section = section_cls

        config = """
        [section]
        [section_1]
        [section_2]
        """

        schema = Schema(self.load_conf(config))
        self.assertEqual(schema.section['1'].suffix, '1')

    def test_get_serialized(self):
        class Schema(ConfigSchema):
            section = TemplateSection(opt1=StringOption('opt1'),
                                      opt2=StringOption('opt2'))

        config = """
        [section]
        opt1 = value_opt1
        [section_another]
        opt2 = value_opt2
        """
        schema = Schema(self.load_conf(config))

        self.assertEqual(schema.section.get_serialized('opt1'), 'value_opt1')
        self.assertEqual(schema.section['another'].get_serialized('opt1'),
                         'value_opt1')
        self.assertEqual(schema.section['another'].get_serialized('opt2'),
                         'value_opt2')
        self.assertRaises(ConfigParser.NoOptionError,
                          schema.section.get_serialized, 'opt2')
        self.assertRaises(ConfigParser.NoOptionError,
                          schema.section.get_serialized, 'opt3')
        self.assertRaises(ConfigParser.NoOptionError,
                          schema.section['another'].get_serialized,
                          'opt3')

    def test_get_serialized_noparent(self):
        class Schema(ConfigSchema):
            section = TemplateSection(opt1=StringOption('opt1'),
                                      opt2=StringOption('opt2'))

        config = """
        [section_another]
        opt2=value_opt2
        """

        schema = Schema(self.load_conf(config))
        self.assertRaises(ConfigParser.NoSectionError,
                          schema.section['another'].get_serialized,
                          'opt1')

    def test_eq(self):
        section = TemplateSection(prefix="section")
        section1 = TemplateSection(prefix="section")

        self.assertEqual(section, section)
        self.assertEqual(section, section1)
        self.assertEqual(section['1'], section['1'])

        section.bind(schema=ConfigSchema(None))
        section1.bind(schema=ConfigSchema(None))

        self.assertEqual(section, section)
        self.assertNotEqual(section, section1)

    def test_match_name(self):
        section = TemplateSection(prefix="section")
        self.assertEqual(section.match_name('section'), section)
        self.assertEqual(section.match_name('section_1'), section['1'])
        self.assertIsNone(section.match_name('notme'))
