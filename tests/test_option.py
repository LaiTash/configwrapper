import io
from unittest import TestCase
from configwrapper import ValidationError
from configwrapper.options import OptionError
from configwrapper.options.strings import StringOption
from configwrapper.schema import ConfigSchema
from configwrapper.section import ConfigSection
import ConfigParser

__author__ = 'Lai Tash (lai.tash@yandex.ru)'



class TestOption(TestCase):
    def setUp(self):
        self.config = ConfigParser.RawConfigParser()

    def load_conf(self, config):
        config = '\n'.join([item.lstrip() for item in config.split('\n')])
        self.config.readfp(io.BytesIO(config))
        return self.config

    def test_validate(self):
        opt1 = StringOption()
        opt2 = StringOption(nullable=True)
        opt3 = StringOption()
        opt4 = StringOption(nullable=True)

        class Schema(ConfigSchema):
            section = ConfigSection(
                opt1=opt1,
                opt2=opt2,
                opt3=opt3,
                opt4=opt4
            )

        config = """
        [section]
        opt1 = something
        opt2 = something_2
        opt3 =
        opt4 =
        """

        schema = Schema(self.load_conf(config))



        opt1.validate(schema.section)
        opt2.validate(schema.section)
        self.assertRaises(ValidationError, opt3.validate, schema.section)
        opt4.validate(schema.section)

    def test_getmethod_singleton(self):
        opt1=StringOption()

        class Schema(ConfigSchema):
            section = ConfigSection(
                opt1=opt1,
                opt2=StringOption(),
                opt3=StringOption(nullable=True),
                opt4=StringOption(nullable=True),
                opt5=StringOption(),
            )

            section2 = ConfigSection(
                opt1=StringOption(default='default value'),
                opt2=StringOption()
            )

        config = """
        [section]
        opt1=something
        opt2=""
        opt3=''
        opt4=
        """

        schema = Schema(self.load_conf(config))

        self.assertEqual(schema.section.opt1, 'something')
        self.assertRaises(ValidationError, lambda: schema.section.opt2)
        self.assertEqual(schema.section.opt3, '')
        self.assertIsNone(schema.section.opt4)
        self.assertRaises(ValidationError, lambda: schema.section.opt5)
        self.assertEqual(schema.section2.opt1, 'default value')

    def test_setmethod(self):
        class Schema(ConfigSchema):
            section = ConfigSection(
                opt1=StringOption(mutable=True),
                opt2=StringOption(),
                opt3=StringOption(mutable=True, nullable=True)
            )

        config = """
        [section]
        opt3=opt3_value
        """

        schema = Schema(self.load_conf(config))

        schema.section.opt1 = 'opt1_value'
        self.assertEqual(schema.section.opt1, 'opt1_value')
        def _temp(): schema.section.opt2 = 'any_value'
        self.assertRaises(OptionError, _temp)
        schema.section.opt3 = 'opt3_new_value'
        self.assertEqual(schema.section.opt3, 'opt3_new_value')
        def _temp(): schema.section.opt1 = None
        self.assertRaises(OptionError, _temp)
        schema.section.opt3 = None
        self.assertIsNone(schema.section.opt3, None)

