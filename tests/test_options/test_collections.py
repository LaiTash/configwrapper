from unittest import TestCase
import mock
from configwrapper import ValidationError
from configwrapper.options.collections import ListOption
from configwrapper.options.enums import EnumOption
from configwrapper.options.numeric import IntOption
from configwrapper.schema import ConfigSchema
from configwrapper.section import ConfigSection
from tests import load_conf

__author__ = 'Lai Tash (lai.tash@yandex.ru)'


class TestListOption(TestCase):
    def setUp(self):
        pass

    def test_deserialize(self):
        option = ListOption(IntOption)
        self.assertEqual(option.deserialize('1,2,  3', None), [1,2,3])
        option = ListOption(IntOption())
        self.assertEqual(option.deserialize('1,2,  3', None), [1,2,3])
        option = ListOption(IntOption(allow=lambda x: x==2))
        self.assertRaises(ValidationError, option.deserialize, '1,2,3', None)

    def test_serialize(self):
        option = ListOption(IntOption)
        self.assertEqual(option.serialize([1, 2, 3], None), '1, 2, 3')

    def test_getmethod(self):
        class Schema(ConfigSchema):
            class section(ConfigSection):
                opt = ListOption(option=EnumOption(
                    enum={
                        '1': 1,
                        '2': 2,
                        '3': 3
                    }
                ))

        config = """
        [section]
        opt=1,2,3
        """

        self.assertEqual(Schema(load_conf(config)).section.opt, [1, 2, 3])