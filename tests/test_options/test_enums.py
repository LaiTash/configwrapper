from unittest import TestCase
from configwrapper.options.enums import EnumOption


class TestEnumOption(TestCase):
    def setUp(self):
        self.option = EnumOption({
            'key1': 'value1',
            'key2': 'value2'
        })
        self.option_fromlist = EnumOption(['key1', 'key2'])



    def test_serialize(self):
        self.assertEqual(self.option.serialize('value2', None), 'key2')
        self.assertEqual(self.option_fromlist.serialize('key2', None), 'key2')

    def test_deserialize(self):
        self.assertEqual(self.option.deserialize('key2', None), 'value2')
        self.assertEqual(self.option_fromlist.deserialize('key2', None), 'key2')
