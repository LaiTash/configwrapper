from unittest import TestCase
from configwrapper import ValidationError
from configwrapper.options.numeric import IntOption


class TestIntOption(TestCase):
    def test_serialize(self):
        self.assertEqual(IntOption().serialize(123, None), '123')

    def test_deserialize(self):
        self.assertEqual(IntOption().deserialize('123', None), 123)

    def test_validate_value(self):
        self.assertRaises(ValidationError, IntOption()._validate_value, None)
        self.assertRaises(ValidationError,
                          IntOption(allow=lambda i: i>=0)._validate_value,
                          '-1')

    def test_validate_serialized(self):
        self.assertRaises(ValidationError, IntOption()._validate_serialized,
                          'abc')