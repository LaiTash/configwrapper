from unittest import TestCase

from configwrapper.options.relations import SectionOption, ObjectOption
from configwrapper.options.strings import StringOption
from configwrapper.schema import ConfigSchema
from configwrapper.section import ConfigSection, TemplateSection
from tests import load_conf


class TestSectionOption(TestCase):
    def setUp(self):

        config = """
        [car]
        engine=ice
        wheels=plastic
        chairs=skin

        [engine]
        fuel=naquadria

        [engine_steam]
        fuel=water

        [engine_ice]
        fuel=gasoline

        [wheels_plastic]
        material=plastic

        [wheels_wooden]
        material=wood

        [chairs_skin]
        material=skin
        """
        self.config = load_conf(config)

        class ChairSection(TemplateSection):
            material = StringOption()

        chair_section = ChairSection(prefix='chairs')

        class Schema(ConfigSchema):
            chair = chair_section

            class wheels(TemplateSection):
                material=StringOption()

            class engine(TemplateSection):
                fuel=StringOption()

            class car(ConfigSection):
                engine = SectionOption(section='engine')
                wheels = SectionOption()
                chairs = SectionOption(section=chair_section)

        self.schema = Schema(load_conf(config))


    def test_serialize(self):
        pass

    def test_deserialize(self):
        pass

    def test_getmethod(self):
        self.assertEqual(self.schema.car.engine.fuel, 'gasoline')
        self.assertEqual(self.schema.car.wheels.material, 'plastic')
        self.assertEqual(self.schema.car.chairs.material, 'skin')

    def test_validate_value(self):
        pass


class TestObjectOption(TestCase):


    def test_deserialize(self):
        class Schema(ConfigSchema):
            class section(ConfigSection):
                opt = ObjectOption(factories={
                    'int': lambda section: int(section.value),
                    'bool': lambda section: bool(int(section.value))
                })
                value = StringOption()

        config = """
        [section]
        opt=int
        value=2
        """

        schema = Schema(load_conf(config))

        self.assertEqual(schema.section.opt, 2)

        config = """
        [section]
        opt=bool
        value=0
        """

        schema = Schema(load_conf(config))
        self.assertFalse(schema.section.opt)

        class Schema(ConfigSchema):
            class section(TemplateSection):
                opt = ObjectOption(factories={
                    'int': lambda section: int(section.value),
                    'bool': lambda section: bool(section.value),
                    'me': lambda section: (section, section.value)
                }, default='bool')
                value = StringOption(default='1')

        config = """
        [section]

        [section_another]
        opt = me
        """

        schema = Schema(load_conf(config))

        self.assertTrue(schema.section.opt)
        another = schema.section['another']
        self.assertEqual(another.opt, (another, '1'))