from unittest import TestCase
from configwrapper import ValidationError
from configwrapper.options.strings import StringOption

__author__ = 'Lai Tash (lai.tash@yandex.ru)'



class TestStrOption(TestCase):
    def test_deserialize(self):
        option = StringOption(nullable=True)
        self.assertEqual(option.deserialize('abc', None), 'abc')
        self.assertEqual(option.deserialize("''", None), "")
        self.assertEqual(option.deserialize('""', None), '""')

    def test_serialize(self):
        option = StringOption(nullable=True)
        self.assertEqual(option.serialize('abc', None), 'abc')
        self.assertEqual(option.serialize('""', None), '""')

    def test_validate(self):
        StringOption()._validate_value('abc')
        self.assertRaises(ValidationError, StringOption()._validate_value, 123)