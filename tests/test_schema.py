import io
from unittest import TestCase
import ConfigParser
from mock import MagicMock
from configwrapper import ValidationError
from configwrapper.options.strings import StringOption
from configwrapper.schema import ConfigSchema
from configwrapper.section import ConfigSection, TemplateSection

__author__ = 'Lai Tash (lai.tash@yandex.ru)'


class TestSchemaMeta(TestCase):
    def test_new(self):

        class Schema(ConfigSchema):
            some_section = ConfigSection()
            other_section = ConfigSection().bind(prefix='other')

        schema = Schema(None)

        self.assertEqual(schema.some_section.schema, schema)
        self.assertEqual(schema.some_section.prefix, 'some_section')
        self.assertEqual(schema.other_section.prefix, 'other')

class TestSchema(TestCase):
    def setUp(self):
        self.config = ConfigParser.RawConfigParser()

    def load_conf(self, config):
        config = '\n'.join([item.lstrip() for item in config.split('\n')])
        self.config.readfp(io.BytesIO(config))
        return self.config

    def test_sections(self):
        section_1 = ConfigSection()
        section_2 = ConfigSection()

        class Schema(ConfigSchema):
            some_section_1 = section_1
            some_section_2 = section_2

        schema = Schema(None)
        self.assertItemsEqual([section.config_name for section in
                               schema.defined_sections],
                              ('some_section_1', 'some_section_2'))

    def test_define_as_cls(self):
        class SectionA(TemplateSection):
            opt3 = StringOption()


        class Schema(ConfigSchema):
            class some_section(ConfigSection):
                opt1 = StringOption()
                opt2 = StringOption()

            class other_section(SectionA):
                prefix = 'other'
                opt1 = StringOption()
                opt2 = StringOption()

        schema = Schema(None)
        self.assertEqual(schema.some_section.schema, schema)
        self.assertEqual(schema.some_section.prefix, 'some_section')
        self.assertEqual(schema.other_section.prefix, 'other')

        config = """
        [some_section]
        opt1=opt1_value

        [other]
        opt1=other_opt1_value

        [other_2]
        opt2=other_opt2_value
        opt3=other_opt3_value
        """

        schema = Schema(self.load_conf(config))
        self.assertEqual(schema.some_section.opt1, 'opt1_value')
        self.assertEqual(schema.other_section.opt1, 'other_opt1_value')
        self.assertEqual(schema.other_section['2'].opt1, 'other_opt1_value')
        self.assertEqual(schema.other_section['2'].opt2, 'other_opt2_value')
        self.assertEqual(schema.other_section['2'].opt3, 'other_opt3_value')

        section = schema.other_section
        self.assertEqual(section.opt1, 'other_opt1_value')
        section = schema.other_section
        self.assertEqual(section['2'].opt2, 'other_opt2_value')
        section = schema.other_section['2']
        self.assertEqual(section.opt3, 'other_opt3_value')


    def test_define_as_args(self):
        schema = ConfigSchema(None,
                            some_section=ConfigSection(),
                            other_section=ConfigSection().bind(prefix='other')
                            )
        self.assertEqual(schema.some_section.schema, schema)
        self.assertEqual(schema.some_section.prefix, 'some_section')
        self.assertEqual(schema.other_section.prefix, 'other')

    def test_validate_undefined(self):
        config = """
        [undefined]
        """

        config = self.load_conf(config)

        self.assertRaises(ValidationError, ConfigSchema(config).validate)
        ConfigSchema(config, allow_undefined=True)

    def test_validate(self):
        config = """
        [section]
        """

        class Schema(ConfigSchema):
            section = ConfigSection()

        Schema.section.validate = MagicMock()

        schema = Schema(self.load_conf(config))
        schema.validate()

        self.assertTrue(schema.section.validate.called)